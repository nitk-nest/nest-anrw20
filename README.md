# NeST: Network Stack Tester

This repository contains the scripts and results of NeST paper
accepted at [https://irtf.org/anrw/2020/](https://irtf.org/anrw/2020/).
The paper presents NeST, a python package that handles testbed setup,
testbed configuration, collecting and visualizing data by providing a
user friendly API, addressing common issues involved in conducting
networking experiments.

## Experiments

[Experiment-1](Experiment-1) and [Experiment-2](Experiment-2) contains all the
data and plots for experiments described in the paper.

### Directory structure of experiment

[programs](programs) contains NeST and Flent programs that were used to run the
experiment along with instructions to rerun the experiment.

[results](results) contains the final processed data and plots obtained from
the experiment.