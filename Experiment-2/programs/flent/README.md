## Instructions to run

Ensure that `flent` is installed in your system.

First, setup the gfc topology in network namespaces:
```
$ sudo ./start
```

**NOTE**: Creating and manipulating network namespaces required root access.

To run the experiment, run the command:
```
$ ./run_tests
```

This will generate output in flent's output format. This raw data can be
visualized using `flent-gui`.

Finally, cleanup the built topology by running:
```
$ ./uninstall
```